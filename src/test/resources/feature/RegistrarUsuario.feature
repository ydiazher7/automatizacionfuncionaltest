@Cucumber
Feature: RegistrarUsuario

  Scenario Outline: Registrar Usuario
    Given que estoy en la pagina principal de Mercury Tours
    When hago click en el link Register
    Then me muestra la pantalla de registro
    When ingreso el <usuario> en el campo UserName
    And ingreso la <contraseña> en el campo Password
    And ingreso la <contraseña2> en el campo Confirm Password
    And doy click en el boton SUBMIT
    Then nos mostrara el mensaje Note: Your user name is admin.
    Examples:
      | usuario | contraseña | contraseña2 |
      | admin   | admin      | admin       |
      | henry   | henry      | henry       |
