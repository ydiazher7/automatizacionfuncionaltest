package com.bdd.PageObjet;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.serenitybdd.core.annotations.findby.FindBy;

@DefaultUrl("http://newtours.demoaut.com/")
public class RegistrarUsuarioPage extends PageObject {

    @FindBy(xpath = "/html/body/div/table/tbody/tr/td[2]/table/tbody/tr[2]/td/table/tbody/tr/td[2]/a")
    WebElementFacade linkRegistrar;
    @FindBy(xpath = ".//input[@name='email']")
    WebElementFacade getEscribirUser;
    @FindBy(xpath = ".//input[@name='password']")
    WebElementFacade getEscribirPassword;
    @FindBy(xpath = ".//input[@name='confirmPassword']")
    WebElementFacade getConfirmarPassword;
    @FindBy(xpath = ".//input[@name='register']")
    WebElementFacade submitBtnConfirmar;
    @FindBy(xpath = ".//*[b=' Note: Your user name is admin.' or @b='Your user name is henry.']")
    WebElementFacade mensajeRegistro;

    public RegistrarUsuarioPage clickRegistrar() {
        linkRegistrar.click();
        return this;
    }

    public void setingresaUsuario(String user) {
        getEscribirUser.type(user);
    }

    public void setingresarPassword(String password) {
        getEscribirPassword.type(password);
    }

    public void setConfirmarPassword(String confirmarPassword) {
        getConfirmarPassword.type(confirmarPassword);
    }

    public RegistrarUsuarioPage clickSubmit() {
        submitBtnConfirmar.click();
        return this;
    }

    public String setConfirmarMensaje(String mensaje){
        return findBy(mensaje, mensajeRegistro).getText();
    }

}
