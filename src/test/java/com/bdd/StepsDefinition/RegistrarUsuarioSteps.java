package com.bdd.StepsDefinition;

import com.bdd.PageObjet.RegistrarUsuarioPage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class RegistrarUsuarioSteps {
    @Steps
    RegistrarUsuarioPage RegistrarUserPage;

    @Given("^que estoy en la pagina principal de Mercury Tours$")
    public void que_estoy_en_la_pagina_principal_de_Mercury_Tours() {
        RegistrarUserPage.setDefaultBaseUrl("http://newtours.demoaut.com/");
        RegistrarUserPage.open();
    }

    @When("^hago click en el link Register$")
    public void hago_click_en_el_link_Register() {
        RegistrarUserPage.clickRegistrar();
    }

    @Then("^me muestra la pantalla de registro$")
    public void me_muestra_la_pantalla_de_registro() {

    }

    @When("^ingreso el (.*) en el campo UserName$")
    public void ingreso_el_usuario_admin_en_el_campo_UserName(String Users) {
        RegistrarUserPage.setingresaUsuario(Users);
    }

    @And("^ingreso la (.*) en el campo Password$")
    public void ingreso_la_contraseña_admin_en_el_campo_Password(String password) {
        RegistrarUserPage.setingresarPassword(password);
    }

    @And("^ingreso la (.*) en el campo Confirm Password$")
    public void ingreso_la_contraseña_admin_en_el_campo_Confirm_Password(String confirmarPassword) {
        RegistrarUserPage.setConfirmarPassword(confirmarPassword);
    }

    @When("^doy click en el boton SUBMIT$")
    public void doy_click_en_el_boton_SUBMIT() {
        RegistrarUserPage.clickSubmit();
    }

    @Then("^nos mostrara el mensaje Note: Your user name is admin.\\.$")
    public void nos_mostrara_el_mensaje_Note_Your_user_name_is_admin() {

    }

}
